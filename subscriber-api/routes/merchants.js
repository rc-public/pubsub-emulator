var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send('merchants root');
})

router.post('/', function(req, res, next) {
  const { message } = req.body
  if (!message) res.sendStatus(400)
  else {
    const { data } = message
    const buffer = Buffer.from(data, 'base64').toString('utf-8')
    const dataObj = JSON.parse(buffer.toString())

    console.log(dataObj)
    res.send(dataObj).status(200)
  }
});

module.exports = router;
