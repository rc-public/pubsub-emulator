const { PubSub } = require('@google-cloud/pubsub')

const projectId = 'groove-pubsub-lab'
const pubSubClient = new PubSub({ projectId })

// const subscriptionA = pubSubClient.subscription(`projects/${projectId}/subscriptions/sub-a`)
const subscriptionB = pubSubClient.subscription(`projects/${projectId}/subscriptions/sub-b`)
// const subscriptionC = pubSubClient.subscription(`projects/${projectId}/subscriptions/sub-c`)
// const subscriptionD = pubSubClient.subscription(`projects/${projectId}/subscriptions/sub-d`)
// const subscriptionE = pubSubClient.subscription(`projects/${projectId}/subscriptions/sub-e`)

const messageHandler = (message) => {
  console.log(`Received message ${message.id}:`)
  console.log(`\tData: ${message.data}`)
  console.log(`\tAttributes:${JSON.stringify(message.attributes)}`)

  // "Ack" (acknowledge receipt of) the message
  message.ack()
}
// subscriptionA.on('message', messageHandler)
subscriptionB.on('message', messageHandler)
// subscriptionC.on('message', messageHandler)
// subscriptionD.on('message', messageHandler)
// subscriptionE.on('message', messageHandler)
