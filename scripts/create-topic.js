const {PubSub} = require('@google-cloud/pubsub')

const projectId = 'project-test'
const topicName = 'topic1'
const subscriptionName = 'topic1-push-sub'
const subscriptionPushEndpoint = 'http://localhost:3000/merchants'

const pubsub = new PubSub({ projectId })


const main = async () => {
  console.log(pubsub)

  console.log('Listing topics...')
  const [topics] = await pubsub.getTopics()
  console.log(`Listing topics ${topics} ... DONE`)
  console.log(`Topics ${topics}`)

  console.log(`Creating topic ${topicName} ...`)
  const resp = await pubsub.createTopic(topicName)
  console.log(`Creating topic ${topicName} ... DONE`)
  console.log(`Resp: ${resp}`)
  
  const options = {
    pushConfig: {
      pushEndpoint: subscriptionPushEndpoint,
    },
  }
  
  console.log(`Creating subscription ${subscriptionName} for ${topicName}...`)
  await pubsub.topic(topicName).createSubscription(subscriptionName, options)
  console.log(`Creating subscription ${subscriptionName} for ${topicName}... DONE`)
}

main()