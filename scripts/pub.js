const {PubSub} = require('@google-cloud/pubsub')
const { format } = require('date-fns')

const projectId = 'project-test'
const topicName = 'topic1'

const pubsub = new PubSub({ projectId })


const main = async () => {
  console.log('Listing topics...')
  const [topics] = await pubsub.getTopics()
  console.log(`Listing topics ${topics} ... DONE`)
  console.log(`Topics ${topics}`)

  console.log(`Getting topic ${topicName} ...`)
  const topic = pubsub.topic(topicName)
  console.log(`Getting topic ${topicName} ... DONE`)
  console.log(`Topic: ${topic}`)
  
  const message = {
    method: 'CREATE',
    sent_at: format(Date.now(), 'yyyy-MM-ddTHH:mm:ss.SSSz'),
    merchant_id: 'n07gf-9g108dg127gd-120d8',
  }
  const messageBuffer = Buffer.from(JSON.stringify(message))
  
  console.log(`Publishing message to ${topicName}...`)
  const messageId = await topic.publish(messageBuffer)
  console.log(`Publishing message to ${topicName}... DONE`)
  
  console.log(`Message ID ${messageId}`)
}

main()